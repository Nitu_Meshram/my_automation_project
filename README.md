# my_automation_project
For automating API's using postman application we can create a personal workspace or team workspace and can work collectively with a team on particular API project.

For the project I was working I have created personal workspace. Then created a collection and added respective API requests in that collection. 

Trigger the API and once response is generated then fetch it into local variable and parse it. Then validated the statuscode and response parameters using functions and properties available in pm library. 

Make use of various variables option available in Postman. Like Environment variables to save time and effort, for random data generation  used Postman Dynamic variables, Collection variables etc.

In case of API failing not getting correct statuscode due to unstable environment, retried API for fix no of time. Created a test script for the same.

For data driven testing uploaded .csv and .json file while running API using collection runner.

Report generated in postman are not understandable to non technical person, so user friendly html and html-extra report generated using newman CLI tool.

